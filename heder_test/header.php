<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="images/Logo-icon.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="style.css">
<meta name="description" content="<?php
if(isset($metaD) && !empty($metaD)) { 
   echo $metaD; 
} 
else { 
   echo ""; 
} ?>" />
<title>
	<?php 
		if(isset($title) && !empty($title)) { 
		   echo $title; 
		} 
		else { 
		   echo "Default title tag"; 
		}
	?>
</title>
</head>
<body>
<header>
<nav>
	<div class="topnav">
		<a href="index.php">Home</a>
		<a href="second.php">Second</a>
		<a href="third.php">Third</a>
	</div>
</nav>
</header>
<div class="container">